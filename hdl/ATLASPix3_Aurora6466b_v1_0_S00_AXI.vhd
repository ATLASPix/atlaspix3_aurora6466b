library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ATLASPix3_Aurora6466b_v1_0_S00_AXI is
	generic (
		-- Users to add parameters here

		-- User parameters ends
		-- Do not modify the parameters beyond this line

		-- Width of S_AXI data bus
		C_S_AXI_DATA_WIDTH	: integer	:= 32;
		-- Width of S_AXI address bus
		C_S_AXI_ADDR_WIDTH	: integer	:= 6
	);
	port (
		-- Users to add ports here
        rxp : in std_logic_vector(0 downto 0);
        rxn : in std_logic_vector(0 downto 0);
        txp : out  std_logic_vector(0 downto 0);
        txn : out  std_logic_vector(0 downto 0);
        gt_refclk_p : in std_logic;
        gt_refclk_n : in std_logic;
        trigger_re : in std_logic;
        timestamp : std_logic_vector(47 downto 0);
	    trigger_timestamp : std_logic_vector(47 downto 0);
        busy : out std_logic;
        init_clk : in std_logic;
--		drp_axi_awaddr : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        drp_axi_rresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
--        drp_axi_bresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
--        drp_axi_wstrb : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
--        drp_axi_wdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        drp_axi_araddr : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        drp_axi_rdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
--        drp_axi_bready : IN STD_LOGIC;
--        drp_axi_awvalid : IN STD_LOGIC;
--        drp_axi_awready : OUT STD_LOGIC;
--        drp_axi_wvalid : IN STD_LOGIC;
--        drp_axi_wready : OUT STD_LOGIC;
--        drp_axi_bvalid : OUT STD_LOGIC;
--        drp_axi_arvalid : IN STD_LOGIC;
--        drp_axi_arready : OUT STD_LOGIC;
--        drp_axi_rvalid : OUT STD_LOGIC;
--        drp_axi_rready : IN STD_LOGIC;
		
		
		-- User ports ends
		-- Do not modify the ports beyond this line

		-- Global Clock Signal
		S_AXI_ACLK	: in std_logic;
		-- Global Reset Signal. This Signal is Active LOW
		S_AXI_ARESETN	: in std_logic;
		-- Write address (issued by master, acceped by Slave)
		S_AXI_AWADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		-- Write channel Protection type. This signal indicates the
    		-- privilege and security level of the transaction, and whether
    		-- the transaction is a data access or an instruction access.
		S_AXI_AWPROT	: in std_logic_vector(2 downto 0);
		-- Write address valid. This signal indicates that the master signaling
    		-- valid write address and control information.
		S_AXI_AWVALID	: in std_logic;
		-- Write address ready. This signal indicates that the slave is ready
    		-- to accept an address and associated control signals.
		S_AXI_AWREADY	: out std_logic;
		-- Write data (issued by master, acceped by Slave) 
		S_AXI_WDATA	: in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		-- Write strobes. This signal indicates which byte lanes hold
    		-- valid data. There is one write strobe bit for each eight
    		-- bits of the write data bus.    
		S_AXI_WSTRB	: in std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
		-- Write valid. This signal indicates that valid write
    		-- data and strobes are available.
		S_AXI_WVALID	: in std_logic;
		-- Write ready. This signal indicates that the slave
    		-- can accept the write data.
		S_AXI_WREADY	: out std_logic;
		-- Write response. This signal indicates the status
    		-- of the write transaction.
		S_AXI_BRESP	: out std_logic_vector(1 downto 0);
		-- Write response valid. This signal indicates that the channel
    		-- is signaling a valid write response.
		S_AXI_BVALID	: out std_logic;
		-- Response ready. This signal indicates that the master
    		-- can accept a write response.
		S_AXI_BREADY	: in std_logic;
		-- Read address (issued by master, acceped by Slave)
		S_AXI_ARADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		-- Protection type. This signal indicates the privilege
    		-- and security level of the transaction, and whether the
    		-- transaction is a data access or an instruction access.
		S_AXI_ARPROT	: in std_logic_vector(2 downto 0);
		-- Read address valid. This signal indicates that the channel
    		-- is signaling valid read address and control information.
		S_AXI_ARVALID	: in std_logic;
		-- Read address ready. This signal indicates that the slave is
    		-- ready to accept an address and associated control signals.
		S_AXI_ARREADY	: out std_logic;
		-- Read data (issued by slave)
		S_AXI_RDATA	: out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		-- Read response. This signal indicates the status of the
    		-- read transfer.
		S_AXI_RRESP	: out std_logic_vector(1 downto 0);
		-- Read valid. This signal indicates that the channel is
    		-- signaling the required read data.
		S_AXI_RVALID	: out std_logic;
		-- Read ready. This signal indicates that the master can
    		-- accept the read data and response information.
		S_AXI_RREADY	: in std_logic
	);
end ATLASPix3_Aurora6466b_v1_0_S00_AXI;

architecture arch_imp of ATLASPix3_Aurora6466b_v1_0_S00_AXI is

	-- AXI4LITE signals
	signal axi_awaddr	: std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
	signal axi_awready	: std_logic;
	signal axi_wready	: std_logic;
	signal axi_bresp	: std_logic_vector(1 downto 0);
	signal axi_bvalid	: std_logic;
	signal axi_araddr	: std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
	signal axi_arready	: std_logic;
	signal axi_rdata	: std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal axi_rresp	: std_logic_vector(1 downto 0);
	signal axi_rvalid	: std_logic;

	-- Example-specific design signals
	-- local parameter for addressing 32 bit / 64 bit C_S_AXI_DATA_WIDTH
	-- ADDR_LSB is used for addressing 32/64 bit registers/memories
	-- ADDR_LSB = 2 for 32 bits (n downto 2)
	-- ADDR_LSB = 3 for 64 bits (n downto 3)
	constant ADDR_LSB  : integer := (C_S_AXI_DATA_WIDTH/32)+ 1;
	constant OPT_MEM_ADDR_BITS : integer := 3;
	------------------------------------------------
	---- Signals for user logic register space example
	--------------------------------------------------
	---- Number of Slave Registers 16
	signal slv_reg0	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg1	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg2	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg3	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg4	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg5	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg6	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg7	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg8	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg9	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg10	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg11	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg12	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg13	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg14	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg15	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal slv_reg_rden	: std_logic;
	signal slv_reg_wren	: std_logic;
	signal reg_data_out	:std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
	signal byte_index	: integer;
	signal aw_en	: std_logic;



signal reset_pb : std_logic;	
signal power_down : std_logic;
signal pma_init : std_logic;
signal drp_clk_in : std_logic;
signal reset2fc : std_logic;
signal user_clk_out : std_logic;
signal reset_fifo : std_logic;

--data 
signal rx_data : std_logic_vector(0 to 63);
signal rx_valid : std_logic;
signal kwrd_data : std_logic_vector(0 to 63);
signal kwrd_valid : std_logic;

signal tx_data : std_logic_vector(0 to 63);
signal tvalid : std_logic;
signal tready : std_logic;

--fifo
signal full : std_logic;
signal almostfull : std_logic;
signal empty : std_logic;
signal fifo_wr_en : std_logic;
signal fifo_data_in : std_logic_vector(63 downto 0);
signal fifo_data_out : std_logic_vector(63 downto 0);
signal rd_en : std_logic := '0';
signal wr_en_fsm : std_logic;

--axi register interface
signal readMSB : std_logic := '0';
signal readLSB : std_logic := '0';
signal data_ready : std_logic := '0';


signal gt_rxcdrovrden_in : std_logic := '0';


signal drpaddr_in :  STD_LOGIC_VECTOR(8 DOWNTO 0);
signal drpdi_in :  STD_LOGIC_VECTOR(15 DOWNTO 0);
signal drpen_in :  STD_LOGIC;
signal drpwe_in :  STD_LOGIC;

signal drp_axi_awaddr :  STD_LOGIC_VECTOR(31 DOWNTO 0) := (others =>'0');
signal drp_axi_rresp :  STD_LOGIC_VECTOR(1 DOWNTO 0):= (others =>'0');
signal drp_axi_bresp :  STD_LOGIC_VECTOR(1 DOWNTO 0):= (others =>'0');
signal drp_axi_wstrb :  STD_LOGIC_VECTOR(3 DOWNTO 0):= (others =>'0');
signal drp_axi_wdata :  STD_LOGIC_VECTOR(31 DOWNTO 0):= (others =>'0');
signal drp_axi_araddr :  STD_LOGIC_VECTOR(31 DOWNTO 0):= (others =>'0');
signal drp_axi_rdata :  STD_LOGIC_VECTOR(31 DOWNTO 0):= (others =>'0');
signal drp_axi_bready :  STD_LOGIC := '0';
signal drp_axi_awvalid :  STD_LOGIC:= '0';
signal drp_axi_awready :  STD_LOGIC:= '0';
signal drp_axi_wvalid :  STD_LOGIC:= '0';
signal drp_axi_wready :  STD_LOGIC:= '0';
signal drp_axi_bvalid :  STD_LOGIC:= '0';
signal drp_axi_arvalid :  STD_LOGIC:= '0';
signal drp_axi_arready :  STD_LOGIC:= '0';
signal drp_axi_rvalid :  STD_LOGIC:= '0';
signal drp_axi_rready :  STD_LOGIC:= '0';

signal data_sim : std_logic_vector(63 downto 0) := (others => '0');
signal read_data_sim : std_logic := '0';
signal rst_data_sim : std_logic := '0';
signal user_clk : std_logic := '0';

signal mmcm_not_locked : std_logic;
signal mmcm_locked_pulse : std_logic := '0';

signal axi_read_strobe : std_logic := '0';
signal axi_read_reg0 : std_logic := '0';
signal axi_read_reg1 : std_logic := '0';
signal fifo_data_read : std_logic := '0';

signal gt_pll_lock_tx : std_logic;
signal gt_pll_lock_rx : std_logic;
signal tx_lane_up : std_logic_vector(0 downto 0);
signal rx_lane_up : std_logic_vector(0 downto 0);



type state_type is (WAIT_READ,GET_NEW_DATA); 
signal state : state_type;
signal fifo_out_valid : std_logic := '0';

signal gt_qpllclk_quad1 : std_logic;
signal gt_qpllrefclk_quad1 : std_logic;
signal gt_refclk1 : std_logic;

COMPONENT aurora_64b66b_tx_sim
  PORT (
    reset_pb : IN STD_LOGIC;
    power_down : IN STD_LOGIC;
    pma_init : IN STD_LOGIC;
    txp : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    txn : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    --tx_hard_err : OUT STD_LOGIC;
    --tx_soft_err : OUT STD_LOGIC;
    --tx_channel_up : OUT STD_LOGIC;
    tx_lane_up : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    --tx_out_clk : OUT STD_LOGIC;
    drp_clk_in : IN STD_LOGIC;
    gt_pll_lock : OUT STD_LOGIC;
    s_axi_tx_tdata : IN STD_LOGIC_VECTOR(63 downto 0);
    s_axi_tx_tvalid : IN STD_LOGIC;
    s_axi_tx_tready : OUT STD_LOGIC;
    mmcm_not_locked_out : OUT STD_LOGIC;
    drpaddr_in : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
    drpdi_in : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    drpen_in : IN STD_LOGIC;
    drpwe_in : IN STD_LOGIC;
    gt_refclk1_p : IN STD_LOGIC;
    gt_refclk1_n : IN STD_LOGIC;
    user_clk_out : OUT STD_LOGIC;
    --sync_clk_out : OUT STD_LOGIC;
    init_clk: IN STD_LOGIC;
    gt_qpllclk_quad1_out : OUT STD_LOGIC;
    gt_qpllrefclk_quad1_out : OUT STD_LOGIC;
    gt_rxcdrovrden_in : IN STD_LOGIC;
    --reset2fg : OUT STD_LOGIC;
    --sys_reset_out : OUT STD_LOGIC;
    --gt_reset_out : OUT STD_LOGIC;
    gt_refclk1_out : OUT STD_LOGIC
  );
END COMPONENT;

COMPONENT aurora_64b66b_0
PORT (
    rxp : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    rxn : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    refclk1_in : IN STD_LOGIC;
    user_clk : IN STD_LOGIC;
    reset_pb : IN STD_LOGIC;
    power_down : IN STD_LOGIC;
    pma_init : IN STD_LOGIC;
    rx_hard_err : OUT STD_LOGIC;
    rx_soft_err : OUT STD_LOGIC;
    rx_channel_up : OUT STD_LOGIC;
    rx_lane_up : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    tx_out_clk : OUT STD_LOGIC;
    drp_clk_in : IN STD_LOGIC;
    gt_pll_lock : OUT STD_LOGIC;
    m_axi_rx_tdata : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    m_axi_rx_tvalid : OUT STD_LOGIC;
    m_axi_rx_user_k_tdata : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    m_axi_rx_user_k_tvalid : OUT STD_LOGIC;
    mmcm_not_locked : IN STD_LOGIC;
    s_axi_awaddr : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    s_axi_rresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    s_axi_bresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    s_axi_wstrb : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    s_axi_wdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    s_axi_araddr : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    s_axi_rdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    s_axi_bready : IN STD_LOGIC;
    s_axi_awvalid : IN STD_LOGIC;
    s_axi_awready : OUT STD_LOGIC;
    s_axi_wvalid : IN STD_LOGIC;
    s_axi_wready : OUT STD_LOGIC;
    s_axi_bvalid : OUT STD_LOGIC;
    s_axi_arvalid : IN STD_LOGIC;
    s_axi_arready : OUT STD_LOGIC;
    s_axi_rvalid : OUT STD_LOGIC;
    s_axi_rready : IN STD_LOGIC;
    init_clk : IN STD_LOGIC;
    link_reset_out : OUT STD_LOGIC;
    gt_qpllclk_quad1_in : IN STD_LOGIC;
    gt_qpllrefclk_quad1_in : IN STD_LOGIC;
    gt_rxcdrovrden_in : IN STD_LOGIC;
    reset2fc : OUT STD_LOGIC;
    sys_reset_out : OUT STD_LOGIC
  );
END COMPONENT;


COMPONENT fifo_generator_0
  PORT (
    wr_clk : IN STD_LOGIC;
    rd_clk : IN STD_LOGIC;
    rst : IN STD_LOGIC;
    din : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    wr_en : IN STD_LOGIC;
    rd_en : IN STD_LOGIC;
    dout : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    full : OUT STD_LOGIC;
    almost_full : OUT STD_LOGIC;
    empty : OUT STD_LOGIC;
    valid : OUT STD_LOGIC
  );
END COMPONENT;

COMPONENT FIFOwrite_FSM
    PORT (
   clk : in std_logic;
   data_in : in STD_LOGIC_VECTOR (63 downto 0);
   data_valid : in std_logic;
   data_out : out STD_LOGIC_VECTOR (63 downto 0);
   wr_en : out STD_LOGIC;
   almostfull : in STD_LOGIC;
   empty : in STD_LOGIC;
   trigger_re : in STD_LOGIC;
   busy : out std_logic;
   timestamp : in STD_LOGIC_VECTOR (47 downto 0);
   trigger_timestamp : in STD_LOGIC_VECTOR (47 downto 0)  
);
END COMPONENT;

COMPONENT generator
    PORT(
        -- Sys connect
    rst_i            : in  std_logic;
    clk_i            : in  std_logic;
    -- Inputs
    read_i           : in  std_logic;
    -- Outputs
    data_o           : out std_logic_vector(63 downto 0)
);   
END COMPONENT;


COMPONENT edge_detection 
     Port ( clk : in STD_LOGIC;
            I : in STD_LOGIC;
            pulse : out STD_LOGIC
);
 end COMPONENT; 



begin
-- I/O Connections assignments

	S_AXI_AWREADY	<= axi_awready;
	S_AXI_WREADY	<= axi_wready;
	S_AXI_BRESP	<= axi_bresp;
	S_AXI_BVALID	<= axi_bvalid;
	S_AXI_ARREADY	<= axi_arready;
	S_AXI_RDATA	<= axi_rdata;
	S_AXI_RRESP	<= axi_rresp;
	S_AXI_RVALID	<= axi_rvalid;
	-- Implement axi_awready generation
	-- axi_awready is asserted for one S_AXI_ACLK clock cycle when both
	-- S_AXI_AWVALID and S_AXI_WVALID are asserted. axi_awready is
	-- de-asserted when reset is low.

	process (S_AXI_ACLK)
	begin
	  if rising_edge(S_AXI_ACLK) then 
	    if S_AXI_ARESETN = '0' then
	      axi_awready <= '0';
	      aw_en <= '1';
	    else
	      if (axi_awready = '0' and S_AXI_AWVALID = '1' and S_AXI_WVALID = '1' and aw_en = '1') then
	        -- slave is ready to accept write address when
	        -- there is a valid write address and write data
	        -- on the write address and data bus. This design 
	        -- expects no outstanding transactions. 
	           axi_awready <= '1';
	           aw_en <= '0';
	        elsif (S_AXI_BREADY = '1' and axi_bvalid = '1') then
	           aw_en <= '1';
	           axi_awready <= '0';
	      else
	        axi_awready <= '0';
	      end if;
	    end if;
	  end if;
	end process;

	-- Implement axi_awaddr latching
	-- This process is used to latch the address when both 
	-- S_AXI_AWVALID and S_AXI_WVALID are valid. 

	process (S_AXI_ACLK)
	begin
	  if rising_edge(S_AXI_ACLK) then 
	    if S_AXI_ARESETN = '0' then
	      axi_awaddr <= (others => '0');
	    else
	      if (axi_awready = '0' and S_AXI_AWVALID = '1' and S_AXI_WVALID = '1' and aw_en = '1') then
	        -- Write Address latching
	        axi_awaddr <= S_AXI_AWADDR;
	      end if;
	    end if;
	  end if;                   
	end process; 

	-- Implement axi_wready generation
	-- axi_wready is asserted for one S_AXI_ACLK clock cycle when both
	-- S_AXI_AWVALID and S_AXI_WVALID are asserted. axi_wready is 
	-- de-asserted when reset is low. 

	process (S_AXI_ACLK)
	begin
	  if rising_edge(S_AXI_ACLK) then 
	    if S_AXI_ARESETN = '0' then
	      axi_wready <= '0';
	    else
	      if (axi_wready = '0' and S_AXI_WVALID = '1' and S_AXI_AWVALID = '1' and aw_en = '1') then
	          -- slave is ready to accept write data when 
	          -- there is a valid write address and write data
	          -- on the write address and data bus. This design 
	          -- expects no outstanding transactions.           
	          axi_wready <= '1';
	      else
	        axi_wready <= '0';
	      end if;
	    end if;
	  end if;
	end process; 

	-- Implement memory mapped register select and write logic generation
	-- The write data is accepted and written to memory mapped registers when
	-- axi_awready, S_AXI_WVALID, axi_wready and S_AXI_WVALID are asserted. Write strobes are used to
	-- select byte enables of slave registers while writing.
	-- These registers are cleared when reset (active low) is applied.
	-- Slave register write enable is asserted when valid address and data are available
	-- and the slave is ready to accept the write address and write data.
	slv_reg_wren <= axi_wready and S_AXI_WVALID and axi_awready and S_AXI_AWVALID ;

	process (S_AXI_ACLK)
	variable loc_addr :std_logic_vector(OPT_MEM_ADDR_BITS downto 0); 
	begin
	  if rising_edge(S_AXI_ACLK) then 
	    if S_AXI_ARESETN = '0' then
	      --slv_reg0 <= (others => '0');
	      --slv_reg1 <= (others => '0');
	      --slv_reg2 <= (others => '0');
	      slv_reg3 <= (others => '0');
	      --slv_reg4 <= (others => '0');
	      slv_reg5 <= (others => '0');
	      slv_reg6 <= (others => '0');
	      slv_reg7 <= (others => '0');
	      slv_reg8 <= (others => '0');
	      slv_reg9 <= (others => '0');
	      slv_reg10 <= (others => '0');
	      slv_reg11 <= (others => '0');
	      slv_reg12 <= (others => '0');
	      slv_reg13 <= (others => '0');
	      slv_reg14 <= (others => '0');
	      slv_reg15 <= (others => '0');
	    else
	      loc_addr := axi_awaddr(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB);
	      if (slv_reg_wren = '1') then
	        case loc_addr is
	          when b"0000" =>
	            for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
	              if ( S_AXI_WSTRB(byte_index) = '1' ) then
	                -- Respective byte enables are asserted as per write strobes                   
	                -- slave registor 0
	                --slv_reg0(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
	              end if;
	            end loop;
	          when b"0001" =>
	            for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
	              if ( S_AXI_WSTRB(byte_index) = '1' ) then
	                -- Respective byte enables are asserted as per write strobes                   
	                -- slave registor 1
	                --slv_reg1(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
	              end if;
	            end loop;
	          when b"0010" =>
	            for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
	              if ( S_AXI_WSTRB(byte_index) = '1' ) then
	                -- Respective byte enables are asserted as per write strobes                   
	                -- slave registor 2
	                --slv_reg2(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
	              end if;
	            end loop;
	          when b"0011" =>
	            for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
	              if ( S_AXI_WSTRB(byte_index) = '1' ) then
	                -- Respective byte enables are asserted as per write strobes                   
	                -- slave registor 3
	                slv_reg3(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
	              end if;
	            end loop;
	          when b"0100" =>
	            for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
	              if ( S_AXI_WSTRB(byte_index) = '1' ) then
	                -- Respective byte enables are asserted as per write strobes                   
	                -- slave registor 4
	                --slv_reg4(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
	              end if;
	            end loop;
	          when b"0101" =>
	            for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
	              if ( S_AXI_WSTRB(byte_index) = '1' ) then
	                -- Respective byte enables are asserted as per write strobes                   
	                -- slave registor 5
	                slv_reg5(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
	              end if;
	            end loop;
	          when b"0110" =>
	            for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
	              if ( S_AXI_WSTRB(byte_index) = '1' ) then
	                -- Respective byte enables are asserted as per write strobes                   
	                -- slave registor 6
	                slv_reg6(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
	              end if;
	            end loop;
	          when b"0111" =>
	            for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
	              if ( S_AXI_WSTRB(byte_index) = '1' ) then
	                -- Respective byte enables are asserted as per write strobes                   
	                -- slave registor 7
	                slv_reg7(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
	              end if;
	            end loop;
	          when b"1000" =>
	            for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
	              if ( S_AXI_WSTRB(byte_index) = '1' ) then
	                -- Respective byte enables are asserted as per write strobes                   
	                -- slave registor 8
	                slv_reg8(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
	              end if;
	            end loop;
	          when b"1001" =>
	            for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
	              if ( S_AXI_WSTRB(byte_index) = '1' ) then
	                -- Respective byte enables are asserted as per write strobes                   
	                -- slave registor 9
	                slv_reg9(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
	              end if;
	            end loop;
	          when b"1010" =>
	            for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
	              if ( S_AXI_WSTRB(byte_index) = '1' ) then
	                -- Respective byte enables are asserted as per write strobes                   
	                -- slave registor 10
	                slv_reg10(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
	              end if;
	            end loop;
	          when b"1011" =>
	            for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
	              if ( S_AXI_WSTRB(byte_index) = '1' ) then
	                -- Respective byte enables are asserted as per write strobes                   
	                -- slave registor 11
	                slv_reg11(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
	              end if;
	            end loop;
	          when b"1100" =>
	            for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
	              if ( S_AXI_WSTRB(byte_index) = '1' ) then
	                -- Respective byte enables are asserted as per write strobes                   
	                -- slave registor 12
	                slv_reg12(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
	              end if;
	            end loop;
	          when b"1101" =>
	            for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
	              if ( S_AXI_WSTRB(byte_index) = '1' ) then
	                -- Respective byte enables are asserted as per write strobes                   
	                -- slave registor 13
	                slv_reg13(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
	              end if;
	            end loop;
	          when b"1110" =>
	            for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
	              if ( S_AXI_WSTRB(byte_index) = '1' ) then
	                -- Respective byte enables are asserted as per write strobes                   
	                -- slave registor 14
	                slv_reg14(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
	              end if;
	            end loop;
	          when b"1111" =>
	            for byte_index in 0 to (C_S_AXI_DATA_WIDTH/8-1) loop
	              if ( S_AXI_WSTRB(byte_index) = '1' ) then
	                -- Respective byte enables are asserted as per write strobes                   
	                -- slave registor 15
	                slv_reg15(byte_index*8+7 downto byte_index*8) <= S_AXI_WDATA(byte_index*8+7 downto byte_index*8);
	              end if;
	            end loop;
	          when others =>
	            --slv_reg0 <= slv_reg0;
	            --slv_reg1 <= slv_reg1;
	            --slv_reg2 <= slv_reg2;
	            slv_reg3 <= slv_reg3;
	            --slv_reg4 <= slv_reg4;
	            slv_reg5 <= slv_reg5;
	            slv_reg6 <= slv_reg6;
	            slv_reg7 <= slv_reg7;
	            slv_reg8 <= slv_reg8;
	            slv_reg9 <= slv_reg9;
	            slv_reg10 <= slv_reg10;
	            slv_reg11 <= slv_reg11;
	            slv_reg12 <= slv_reg12;
	            slv_reg13 <= slv_reg13;
	            slv_reg14 <= slv_reg14;
	            slv_reg15 <= slv_reg15;
	        end case;
	      end if;
	    end if;
	  end if;                   
	end process; 

	-- Implement write response logic generation
	-- The write response and response valid signals are asserted by the slave 
	-- when axi_wready, S_AXI_WVALID, axi_wready and S_AXI_WVALID are asserted.  
	-- This marks the acceptance of address and indicates the status of 
	-- write transaction.

	process (S_AXI_ACLK)
	begin
	  if rising_edge(S_AXI_ACLK) then 
	    if S_AXI_ARESETN = '0' then
	      axi_bvalid  <= '0';
	      axi_bresp   <= "00"; --need to work more on the responses
	    else
	      if (axi_awready = '1' and S_AXI_AWVALID = '1' and axi_wready = '1' and S_AXI_WVALID = '1' and axi_bvalid = '0'  ) then
	        axi_bvalid <= '1';
	        axi_bresp  <= "00"; 
	      elsif (S_AXI_BREADY = '1' and axi_bvalid = '1') then   --check if bready is asserted while bvalid is high)
	        axi_bvalid <= '0';                                 -- (there is a possibility that bready is always asserted high)
	      end if;
	    end if;
	  end if;                   
	end process; 

	-- Implement axi_arready generation
	-- axi_arready is asserted for one S_AXI_ACLK clock cycle when
	-- S_AXI_ARVALID is asserted. axi_awready is 
	-- de-asserted when reset (active low) is asserted. 
	-- The read address is also latched when S_AXI_ARVALID is 
	-- asserted. axi_araddr is reset to zero on reset assertion.

	process (S_AXI_ACLK)
	begin
	  if rising_edge(S_AXI_ACLK) then 
	    if S_AXI_ARESETN = '0' then
	      axi_arready <= '0';
	      axi_araddr  <= (others => '1');
	    else
	      if (axi_arready = '0' and S_AXI_ARVALID = '1') then
	        -- indicates that the slave has acceped the valid read address
	        axi_arready <= '1';
	        -- Read Address latching 
	        axi_araddr  <= S_AXI_ARADDR;           
	      else
	        axi_arready <= '0';
	      end if;
	    end if;
	  end if;                   
	end process; 

	-- Implement axi_arvalid generation
	-- axi_rvalid is asserted for one S_AXI_ACLK clock cycle when both 
	-- S_AXI_ARVALID and axi_arready are asserted. The slave registers 
	-- data are available on the axi_rdata bus at this instance. The 
	-- assertion of axi_rvalid marks the validity of read data on the 
	-- bus and axi_rresp indicates the status of read transaction.axi_rvalid 
	-- is deasserted on reset (active low). axi_rresp and axi_rdata are 
	-- cleared to zero on reset (active low).  
	process (S_AXI_ACLK)
	begin
	  if rising_edge(S_AXI_ACLK) then
	    if S_AXI_ARESETN = '0' then
	      axi_rvalid <= '0';
	      axi_rresp  <= "00";
	      axi_read_strobe <= '0';
	    else
	      axi_read_strobe <= '0';
	      if (axi_arready = '1' and S_AXI_ARVALID = '1' and axi_rvalid = '0') then
	        -- Valid read data is available at the read data bus
	        axi_rvalid <= '1';
	        axi_rresp  <= "00"; -- 'OKAY' response
	      elsif (axi_rvalid = '1' and S_AXI_RREADY = '1') then
	        -- Read data is accepted by the master
	        axi_rvalid <= '0';
	        axi_read_strobe <='1';
	      end if;            
	    end if;
	  end if;
	end process;

	-- Implement memory mapped register select and read logic generation
	-- Slave register read enable is asserted when valid address is available
	-- and the slave is ready to accept the read address.
	slv_reg_rden <= axi_arready and S_AXI_ARVALID and (not axi_rvalid) ;

	process (slv_reg0, slv_reg1, slv_reg2, slv_reg3, slv_reg4, slv_reg5, slv_reg6, slv_reg7, slv_reg8, slv_reg9, slv_reg10, slv_reg11, slv_reg12, slv_reg13, slv_reg14, slv_reg15, axi_araddr, S_AXI_ARESETN, slv_reg_rden)
	variable loc_addr :std_logic_vector(OPT_MEM_ADDR_BITS downto 0);
	begin
	    -- Address decoding for reading registers
	    loc_addr := axi_araddr(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB);
	    case loc_addr is
	      when b"0000" =>
	        reg_data_out <= slv_reg0;
	      when b"0001" =>
	        reg_data_out <= slv_reg1;
	      when b"0010" =>
	        reg_data_out <= slv_reg2;
	      when b"0011" =>
	        reg_data_out <= slv_reg3;
	      when b"0100" =>
	        reg_data_out <= slv_reg4;
	      when b"0101" =>
	        reg_data_out <= slv_reg5;
	      when b"0110" =>
	        reg_data_out <= slv_reg6;
	      when b"0111" =>
	        reg_data_out <= slv_reg7;
	      when b"1000" =>
	        reg_data_out <= slv_reg8;
	      when b"1001" =>
	        reg_data_out <= slv_reg9;
	      when b"1010" =>
	        reg_data_out <= slv_reg10;
	      when b"1011" =>
	        reg_data_out <= slv_reg11;
	      when b"1100" =>
	        reg_data_out <= slv_reg12;
	      when b"1101" =>
	        reg_data_out <= slv_reg13;
	      when b"1110" =>
	        reg_data_out <= slv_reg14;
	      when b"1111" =>
	        reg_data_out <= slv_reg15;
	      when others =>
	        reg_data_out  <= (others => '0');
	    end case;
	end process; 

	-- Output register or memory read data
	process( S_AXI_ACLK ) is
	begin
	  if (rising_edge (S_AXI_ACLK)) then
	    if ( S_AXI_ARESETN = '0' ) then
	      axi_rdata  <= (others => '0');
	    else
	      if (slv_reg_rden = '1') then
	        -- When there is a valid read address (S_AXI_ARVALID) with 
	        -- acceptance of read address by the slave (axi_arready), 
	        -- output the read dada 
	        -- Read address mux
	          axi_rdata <= reg_data_out;     -- register read data                  	          
	      end if;   
	    end if;
	  end if;
	end process;


	-- Add user logic here
	

	
	ATP3_RCV : aurora_64b66b_0
  PORT MAP (
    rxp => rxp,
    rxn => rxn,
    reset_pb => reset_pb,
    power_down => power_down,
    pma_init => pma_init,
    drp_clk_in => init_clk,
    m_axi_rx_tdata => rx_data,
    m_axi_rx_tvalid => rx_valid,
    m_axi_rx_user_k_tdata => kwrd_data,
    m_axi_rx_user_k_tvalid => kwrd_valid,
    s_axi_awaddr => drp_axi_awaddr,
    s_axi_rresp => drp_axi_rresp,
    s_axi_bresp => drp_axi_bresp,
    s_axi_wstrb => drp_axi_wstrb,
    s_axi_wdata => drp_axi_wdata,
    s_axi_araddr => drp_axi_araddr,
    s_axi_rdata => drp_axi_rdata,
    s_axi_bready => drp_axi_bready,
    s_axi_awvalid => drp_axi_awvalid,
    s_axi_awready => drp_axi_awready,
    s_axi_wvalid => drp_axi_wvalid,
    s_axi_wready => drp_axi_wready,
    s_axi_bvalid => drp_axi_bvalid,
    s_axi_arvalid => drp_axi_arvalid,
    s_axi_arready => drp_axi_arready,
    s_axi_rvalid => drp_axi_rvalid,
    s_axi_rready => drp_axi_rready,
    init_clk => init_clk,
    gt_qpllclk_quad1_in => gt_qpllclk_quad1,
    gt_qpllrefclk_quad1_in => gt_qpllrefclk_quad1,
    user_clk=> user_clk,
    reset2fc => reset2fc,
    gt_rxcdrovrden_in => gt_rxcdrovrden_in,
    mmcm_not_locked => mmcm_not_locked,
    gt_pll_lock => gt_pll_lock_rx,
    rx_lane_up => rx_lane_up,
    refclk1_in => gt_refclk1
  );

auroratx : aurora_64b66b_tx_sim
  PORT MAP (
    reset_pb => reset_pb,
    power_down => power_down,
    pma_init => pma_init,
    txp => txp,
    txn => txn,
    s_axi_tx_tdata => tx_data,
    s_axi_tx_tvalid => tvalid,
    s_axi_tx_tready => tready,
    drpaddr_in => drpaddr_in,
    drpdi_in => drpdi_in,
    drpen_in => drpen_in,
    drpwe_in => drpwe_in,
    --tx_hard_err => tx_hard_err,
    --tx_soft_err => tx_soft_err,
    --tx_channel_up => tx_channel_up,
    tx_lane_up => tx_lane_up,
    --tx_out_clk => tx_clk,
    drp_clk_in => init_clk,
    gt_pll_lock => gt_pll_lock_tx,
    mmcm_not_locked_out => mmcm_not_locked,
    --link_reset_out => link_reset_out,
    gt_refclk1_p => gt_refclk_p,
    gt_refclk1_n => gt_refclk_n,
    user_clk_out => user_clk,
    --sync_clk_out => sync_clk_out,
    init_clk => init_clk,
    gt_qpllclk_quad1_out => gt_qpllclk_quad1,
    gt_qpllrefclk_quad1_out => gt_qpllrefclk_quad1,
    --init_clk_out => init_clk_out,
    gt_rxcdrovrden_in => gt_rxcdrovrden_in,
    --reset2fg => reset2fg,
    --sys_reset_out => sys_reset_out,
    --gt_reset_out => gt_reset_out,
    gt_refclk1_out => gt_refclk1
  );
  
  
  mmcm_lock_detection : edge_detection
    PORT MAP(
        clk => init_clk,
       I => mmcm_not_locked,
       pulse => mmcm_locked_pulse
);
  
  data_generator : generator
    PORT MAP(
    rst_i   => rst_data_sim ,        
    clk_i   =>  user_clk,          
    read_i  => read_data_sim,        
    data_o  => data_sim         
);
    
tx_data <= data_sim;

read_data_sim <= tready;	
tvalid <= '1';
	
fifo_wr_en <= wr_en_fsm and rx_valid ;




	
fifowr_FSM_inst  : FIFOwrite_FSM
    PORT MAP (
    clk => user_clk_out,
    data_in => rx_data,
    data_valid => rx_valid,
    data_out => fifo_data_in,
    wr_en => wr_en_fsm,
    almostfull => almostfull,
    empty => empty,
    trigger_re => trigger_re,
    busy => busy, 
    timestamp => timestamp,
    trigger_timestamp => trigger_timestamp
    );

data_fifo_inst : fifo_generator_0
  PORT MAP (
    wr_clk => user_clk_out,
    rd_clk => S_AXI_ACLK,
    rst => reset_fifo,
    din => fifo_data_in,
    wr_en => fifo_wr_en,
    rd_en => rd_en,
    dout => fifo_data_out,
    full => full,
    almost_full => almostfull,
    empty => empty,
    valid => fifo_out_valid
  );	
	
	
slv_reg0 <= fifo_data_out(63 downto 32);
slv_reg1 <= fifo_data_out(31 downto 0);
slv_reg2(0) <= data_ready;

reset_pb <= slv_reg3(0);
pma_init <= slv_reg3(1);
power_down <= slv_reg3(2);
rst_data_sim <= slv_reg3(1);
reset_fifo <= slv_reg3(3) or mmcm_locked_pulse;

slv_reg4(0) <= gt_pll_lock_tx;
slv_reg4(1) <= tx_lane_up(0);
slv_reg4(2) <= gt_pll_lock_rx;
slv_reg4(3) <= rx_lane_up(0);

axi_read_reg0 <= '1' when axi_araddr(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB) = b"0000" else '0';
axi_read_reg1 <= '1' when axi_araddr(ADDR_LSB + OPT_MEM_ADDR_BITS downto ADDR_LSB) = b"0001" else '0';
	




NEXT_STATE_DECODE  : process(S_AXI_ACLK)
    begin
    if rising_edge(S_AXI_ACLK) then
    
    case state is 
        when WAIT_READ =>
            if(readMSB = '1' and readLSB = '1' and fifo_out_valid = '1') then 
                state <= GET_NEW_DATA;
            else
                state <= WAIT_READ;
            end if;
        when GET_NEW_DATA => 
            
            if(fifo_out_valid = '0') then 
                state <= GET_NEW_DATA;
            else
                state <= WAIT_READ;
            end if;
        when others =>
                state <= WAIT_READ;
        end case;        
    end if;
    end process;
	
OUTPUT_DECODE  : process(S_AXI_ACLK)
begin
    if rising_edge(S_AXI_ACLK) then	
    case state is 
        when WAIT_READ =>
            data_ready <= '0';
            rd_en <='0';          
            if((readMSB ='0' or readLSB = '0') and fifo_out_valid ='1') then 
                data_ready <= '1';
            end if;          
            if(axi_read_reg0 = '1' and axi_read_strobe = '1') then 
                readMSB <= '1';
            end if;          
            if(axi_read_reg1 = '1' and axi_read_strobe = '1') then 
                readLSB <= '1';
            end if;                    
        when GET_NEW_DATA => 
            rd_en <='1';
            readMSB <= '0';
            readLSB <= '0';
            data_ready <= '0';          
        when others =>
            data_ready <='0';
            readMSB <= '0';
            readLSB <= '0'; 
            rd_en <='0';
              
        end case;        
    end if;
    end process;	
	

	-- User logic ends

end arch_imp;
