library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ATLASPix3_Aurora6466b_v1_0 is
	generic (
		-- Users to add parameters here

		-- User parameters ends
		-- Do not modify the parameters beyond this line


		-- Parameters of Axi Slave Bus Interface S00_AXI
		C_S00_AXI_DATA_WIDTH	: integer	:= 32;
		C_S00_AXI_ADDR_WIDTH	: integer	:= 6
	);
	port (
		-- Users to add ports here
        rxp : in  std_logic_vector(0 downto 0);
        rxn : in  std_logic_vector(0 downto 0);
        txp : out  std_logic_vector(0 downto 0);
        txn : out  std_logic_vector(0 downto 0);        
        gt_refclk_p : in std_logic;
        gt_refclk_n : in std_logic;
        init_clk : in std_logic;

        trigger_re : in std_logic;
        timestamp : std_logic_vector(47 downto 0);
	    trigger_timestamp : std_logic_vector(47 downto 0);
        busy : out std_logic;
                
		-- DRP AXI-Lite
--		drp_axi_awaddr : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        drp_axi_rresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
--        drp_axi_bresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
--        drp_axi_wstrb : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
--        drp_axi_wdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        drp_axi_araddr : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        drp_axi_rdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
--        drp_axi_bready : IN STD_LOGIC;
--        drp_axi_awvalid : IN STD_LOGIC;
--        drp_axi_awready : OUT STD_LOGIC;
--        drp_axi_wvalid : IN STD_LOGIC;
--        drp_axi_wready : OUT STD_LOGIC;
--        drp_axi_bvalid : OUT STD_LOGIC;
--        drp_axi_arvalid : IN STD_LOGIC;
--        drp_axi_arready : OUT STD_LOGIC;
--        drp_axi_rvalid : OUT STD_LOGIC;
--        drp_axi_rready : IN STD_LOGIC;
		
		
		
		-- User ports ends
		-- Do not modify the ports beyond this line


		-- Ports of Axi Slave Bus Interface S00_AXI
		s00_axi_aclk	: in std_logic;
		s00_axi_aresetn	: in std_logic;
		s00_axi_awaddr	: in std_logic_vector(C_S00_AXI_ADDR_WIDTH-1 downto 0);
		s00_axi_awprot	: in std_logic_vector(2 downto 0);
		s00_axi_awvalid	: in std_logic;
		s00_axi_awready	: out std_logic;
		s00_axi_wdata	: in std_logic_vector(C_S00_AXI_DATA_WIDTH-1 downto 0);
		s00_axi_wstrb	: in std_logic_vector((C_S00_AXI_DATA_WIDTH/8)-1 downto 0);
		s00_axi_wvalid	: in std_logic;
		s00_axi_wready	: out std_logic;
		s00_axi_bresp	: out std_logic_vector(1 downto 0);
		s00_axi_bvalid	: out std_logic;
		s00_axi_bready	: in std_logic;
		s00_axi_araddr	: in std_logic_vector(C_S00_AXI_ADDR_WIDTH-1 downto 0);
		s00_axi_arprot	: in std_logic_vector(2 downto 0);
		s00_axi_arvalid	: in std_logic;
		s00_axi_arready	: out std_logic;
		s00_axi_rdata	: out std_logic_vector(C_S00_AXI_DATA_WIDTH-1 downto 0);
		s00_axi_rresp	: out std_logic_vector(1 downto 0);
		s00_axi_rvalid	: out std_logic;
		s00_axi_rready	: in std_logic
	);
end ATLASPix3_Aurora6466b_v1_0;

architecture arch_imp of ATLASPix3_Aurora6466b_v1_0 is

	-- component declaration
	component ATLASPix3_Aurora6466b_v1_0_S00_AXI is
		generic (
		C_S_AXI_DATA_WIDTH	: integer	:= 32;
		C_S_AXI_ADDR_WIDTH	: integer	:= 6
		);
		
		port (
		rxp : in  std_logic_vector(0 downto 0);
        rxn : in  std_logic_vector(0 downto 0);
        txp : out  std_logic_vector(0 downto 0);
        txn : out  std_logic_vector(0 downto 0);
        init_clk : in std_logic;
        gt_refclk_p : in std_logic;
        gt_refclk_n : in std_logic;
        trigger_re : in std_logic;
        timestamp : std_logic_vector(47 downto 0);
	    trigger_timestamp : std_logic_vector(47 downto 0);
        busy : out std_logic;
--        drp_axi_awaddr : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        drp_axi_rresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
--        drp_axi_bresp : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
--        drp_axi_wstrb : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
--        drp_axi_wdata : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        drp_axi_araddr : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--        drp_axi_rdata : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
--        drp_axi_bready : IN STD_LOGIC;
--        drp_axi_awvalid : IN STD_LOGIC;
--        drp_axi_awready : OUT STD_LOGIC;
--        drp_axi_wvalid : IN STD_LOGIC;
--        drp_axi_wready : OUT STD_LOGIC;
--        drp_axi_bvalid : OUT STD_LOGIC;
--        drp_axi_arvalid : IN STD_LOGIC;
--        drp_axi_arready : OUT STD_LOGIC;
--        drp_axi_rvalid : OUT STD_LOGIC;
--        drp_axi_rready : IN STD_LOGIC;
		S_AXI_ACLK	: in std_logic;
		S_AXI_ARESETN	: in std_logic;
		S_AXI_AWADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_AWPROT	: in std_logic_vector(2 downto 0);
		S_AXI_AWVALID	: in std_logic;
		S_AXI_AWREADY	: out std_logic;
		S_AXI_WDATA	: in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_WSTRB	: in std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
		S_AXI_WVALID	: in std_logic;
		S_AXI_WREADY	: out std_logic;
		S_AXI_BRESP	: out std_logic_vector(1 downto 0);
		S_AXI_BVALID	: out std_logic;
		S_AXI_BREADY	: in std_logic;
		S_AXI_ARADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_ARPROT	: in std_logic_vector(2 downto 0);
		S_AXI_ARVALID	: in std_logic;
		S_AXI_ARREADY	: out std_logic;
		S_AXI_RDATA	: out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_RRESP	: out std_logic_vector(1 downto 0);
		S_AXI_RVALID	: out std_logic;
		S_AXI_RREADY	: in std_logic
		);
	end component ATLASPix3_Aurora6466b_v1_0_S00_AXI;

begin

-- Instantiation of Axi Bus Interface S00_AXI
ATLASPix3_Aurora6466b_v1_0_S00_AXI_inst : ATLASPix3_Aurora6466b_v1_0_S00_AXI
	generic map (
		C_S_AXI_DATA_WIDTH	=> C_S00_AXI_DATA_WIDTH,
		C_S_AXI_ADDR_WIDTH	=> C_S00_AXI_ADDR_WIDTH
	)
	port map (
		S_AXI_ACLK	=> s00_axi_aclk,
		S_AXI_ARESETN	=> s00_axi_aresetn,
		S_AXI_AWADDR	=> s00_axi_awaddr,
		S_AXI_AWPROT	=> s00_axi_awprot,
		S_AXI_AWVALID	=> s00_axi_awvalid,
		S_AXI_AWREADY	=> s00_axi_awready,
		S_AXI_WDATA	=> s00_axi_wdata,
		S_AXI_WSTRB	=> s00_axi_wstrb,
		S_AXI_WVALID	=> s00_axi_wvalid,
		S_AXI_WREADY	=> s00_axi_wready,
		S_AXI_BRESP	=> s00_axi_bresp,
		S_AXI_BVALID	=> s00_axi_bvalid,
		S_AXI_BREADY	=> s00_axi_bready,
		S_AXI_ARADDR	=> s00_axi_araddr,
		S_AXI_ARPROT	=> s00_axi_arprot,
		S_AXI_ARVALID	=> s00_axi_arvalid,
		S_AXI_ARREADY	=> s00_axi_arready,
		S_AXI_RDATA	=> s00_axi_rdata,
		S_AXI_RRESP	=> s00_axi_rresp,
		S_AXI_RVALID	=> s00_axi_rvalid,
		S_AXI_RREADY	=> s00_axi_rready,
--		drp_axi_awaddr => drp_axi_awaddr,
--        drp_axi_rresp => drp_axi_rresp,
--        drp_axi_bresp => drp_axi_bresp,
--        drp_axi_wstrb => drp_axi_wstrb,
--        drp_axi_wdata => drp_axi_wdata,
--        drp_axi_araddr => drp_axi_araddr,
--        drp_axi_rdata => drp_axi_rdata,
--        drp_axi_bready => drp_axi_bready,
--        drp_axi_awvalid => drp_axi_awvalid,
--        drp_axi_awready => drp_axi_awready,
--        drp_axi_wvalid => drp_axi_wvalid,
--        drp_axi_wready => drp_axi_wready,
--        drp_axi_bvalid => drp_axi_bvalid,
--        drp_axi_arvalid => drp_axi_arvalid,
--        drp_axi_arready => drp_axi_arready,
--        drp_axi_rvalid => drp_axi_rvalid,
--        drp_axi_rready => drp_axi_rready,
		rxp => rxp,
		rxn => rxn,
		txp => txp,
		txn => txn,
		gt_refclk_p => gt_refclk_p,
		gt_refclk_n => gt_refclk_n,
		trigger_re => trigger_re,
        timestamp  => timestamp,
	    trigger_timestamp => trigger_timestamp,
        busy => busy,
        init_clk => init_clk

	);

	-- Add user logic here

	-- User logic ends

end arch_imp;
