`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/15/2019 09:10:27 AM
// Design Name: 
// Module Name: AURORA_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module AURORA_tb();


//clock and reset_n signals
	reg aclk =1'b0;
	reg arstn = 1'b0;

	
	//Write Address channel (AW)
	reg [31:0] write_addr =32'd0;	//Master write address
	reg [2:0] write_prot = 3'd0;	//type of write(leave at 0)
	reg write_addr_valid = 1'b0;	//master indicating address is valid
	wire write_addr_ready;		//slave ready to receive address

	//Write Data Channel (W)
	reg [31:0] write_data = 32'd0;	//Master write data
	reg [3:0] write_strb = 4'd0;	//Master byte-wise write strobe
	reg write_data_valid = 1'b0;	//Master indicating write data is valid
	wire write_data_ready;		//slave ready to receive data

	//Write Response Channel (WR)
	reg write_resp_ready = 1'b0;	//Master ready to receive write response
	wire [1:0] write_resp;		//slave write response
	wire write_resp_valid;		//slave response valid
	
	//Read Address channel (AR)
	reg [31:0] read_addr = 32'd0;	//Master read address
	reg [2:0] read_prot =3'd0;	//type of read(leave at 0)
	reg read_addr_valid = 1'b0;	//Master indicating address is valid
	wire read_addr_ready;		//slave ready to receive address
   
	//Read Data Channel (R)
	reg read_data_ready = 1'b0;	//Master indicating ready to receive data
	wire [31:0] read_data;		//slave read data
	wire [1:0] read_resp;		//slave read response
	wire read_data_valid;		//slave indicating data in channel is valid
	
	reg reference_clk=1'b1;
    wire atp3_cmd_p;
    wire atp3_cmd_n;
    reg trigger_in=1'b0;
    
    reg clk40 = 1'b1;
    

    wire [0:0] rxp;
    wire [0:0] rxn;

    reg init_clk = 1'b0;
    reg gt_refclk_p = 1'b0;
    reg gt_refclk_n = 1'b1;
    reg aur_clk = 1'b1;
    reg aurdummy_rst = 1'b1;
    reg scram_en = 1'b1;
    
    integer data_cnt = 0;
    
    task axi_read;
      input [31 : 0] addr;
      input [31 : 0] expected_data;
      begin
        read_addr = addr;
        read_addr_valid <= 1'b1;
        read_data_ready <= 1'b1;
        wait(read_addr_ready);
        wait(read_data_valid);
    
        if (read_data != expected_data) begin
          $display("Error: Mismatch in AXI4 read at %x: ", addr,
            "expected %x, received %x",
            expected_data, read_data);
        end
        else 
        begin 
        $display("MATCH: Matching data  for AXI4 read at %x: ", addr,
            "expected %x, received %x",
            expected_data, read_data);
        end
    
        @(posedge aclk) #1;
        read_addr_valid <= 1'b0;
        read_data_ready <= 1'b0;
      end
endtask
    
    
    
    
    task axi_write;
        input [31:0] addr;
        input [31:0] data;
        begin
            #3 write_addr <= addr;    //Put write address on bus
            write_data <= data;    //put write data on bus
            write_addr_valid <= 1'b1;    //indicate address is valid
            write_data_valid <= 1'b1;    //indicate data is valid
            write_resp_ready <= 1'b1;    //indicate ready for a response
            write_strb <= 4'hF;        //writing all 4 bytes
    
            //wait for one slave ready signal or the other
            wait(write_data_ready || write_addr_ready);
                
            @(posedge aclk); //one or both signals and a positive edge
            if(write_data_ready&&write_addr_ready)//received both ready signals
            begin
                write_addr_valid<=0;
                write_data_valid<=0;
            end
            else    //wait for the other signal and a positive edge
            begin
                if(write_data_ready)    //case data handshake completed
                begin
                    write_data_valid<=0;
                    wait(write_addr_ready); //wait for address address ready
                end
                        else if(write_addr_ready)   //case address handshake completed
                        begin
                    write_addr_valid<=0;
                            wait(write_data_ready); //wait for data ready
                        end 
                @ (posedge aclk);// complete the second handshake
                write_addr_valid<=0; //make sure both valid signals are deasserted
                write_data_valid<=0;
            end
                
            //both handshakes have occured
            //deassert strobe
            write_strb<=0;
    
            //wait for valid response
            wait(write_resp_valid);
            
            //both handshake signals and rising edge
            @(posedge aclk);
    
            //deassert ready for response
            write_resp_ready<=0;
    
    
            //end of write transaction
        end
        endtask
    

	//Instantiation of SPI IP
	ATLASPix3_Aurora6466b_v1_0_S00_AXI # (
		.C_S_AXI_DATA_WIDTH(32),
		.C_S_AXI_ADDR_WIDTH(32)
	) ATLASPix3_Aurora6466b_v1_0_S00_AXI (
		.init_clk(init_clk),		
		.rxp(rxp),
		.rxn(rxn),
		.gt_refclk_p(gt_refclk_p),
		.gt_refclk_n(gt_refclk_n),
		
		.s_axi_aclk(aclk),		
		.s_axi_aresetn(arstn),	
		.s_axi_awaddr(write_addr),
		.s_axi_awprot(write_prot),
		.s_axi_awvalid(write_addr_valid),
		.s_axi_awready(write_addr_ready),

		.s_axi_wdata(write_data),
		.s_axi_wstrb(write_strb),
		.s_axi_wvalid(write_data_valid),
		.s_axi_wready(write_data_ready),

		.s_axi_bresp(write_resp),
		.s_axi_bvalid(write_resp_valid),
		.s_axi_bready(write_resp_ready),

		.s_axi_araddr(read_addr),
		.s_axi_arprot(read_prot),
		.s_axi_arvalid(read_addr_valid),
		.s_axi_arready(read_addr_ready),

		.s_axi_rdata(read_data),
		.s_axi_rresp(read_resp),
		.s_axi_rvalid(read_data_valid),
		.s_axi_rready(read_data_ready)
	);

//    dummy_atp3_6466b  dummy_atp3_6466b(
//        .clk(aur_clk),
//        .rxp(rxp),
//        .rxn(rxn),
//        .rst(aurdummy_rst),
//        .scram_en(scram_en)
//    );

    parameter AXI_CLK_PERIOD = 20; 	
    parameter GT_CLK_PERIOD = 3.125;     
    parameter INIT_CLK_PERIOD = 6.25; 
    parameter AUR_CLK_PERIOD = 0.78125;    
    //clock signal     
    always         #(AXI_CLK_PERIOD/2.0) aclk <=~aclk;     
    always         #(INIT_CLK_PERIOD/2.0) init_clk <=~init_clk;      
    always         #(AUR_CLK_PERIOD/2.0) aur_clk <=~aur_clk;      

    always  #(GT_CLK_PERIOD/2.0) gt_refclk_p <= ~gt_refclk_p;
    always  #(GT_CLK_PERIOD/2.0) gt_refclk_n <= ~gt_refclk_n;


    initial     
    
    begin                 
    // resets         
    arstn = 0;         
    #20 arstn=1;
    #20 aurdummy_rst = 1'b0;         
    #20 scram_en = 1'b0;
    #20 axi_write(32'hC,4'b1111); 
    #2000   
    #20 axi_write(32'hC,4'b1011); 
    #2000
    #20 axi_write(32'hC,4'b1001); 
    #2000           
    #20 axi_write(32'hC,4'b0000);
    #280000
    for(data_cnt=0; data_cnt<100; data_cnt=data_cnt+1)
        begin
            #20 axi_read(32'h0,0);
            #20 axi_read(32'h4,data_cnt);
        end;
    end


endmodule
