`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/15/2019 11:47:06 AM
// Design Name: 
// Module Name: dummy_atp3_6466b
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module dummy_atp3_6466b(
    input clk,
    output rxp,
    output rxn,
    input rst,
    input scram_en
    );

integer cnt = 0;
parameter maxcnt = 65;
reg [65:0] IDLEWORD = {2'b10,8'h78,8'b10000000,48'hADA};
//reg [65:0] IDLEWORD = {66'hABCDABCDABCDABCD};

wire [65:0] scrambled ;
wire [65:0] scrambled2 = 66'b100111100010000000000000000000000000000001000011101111010100000100;
integer i = 0;

reg [0:0] rxp_reg = 1;
reg [0:0] rxn_reg = 0;

reg scram_rst = 1'b0;

assign rxp = rxp_reg;
assign rxn = rxn_reg;


scrambler_vlog scrambler(
    .clk(clk),
    .data_in(IDLEWORD[63:0]),
    .rst(rst),
    .enable(scram_en),
    .sync_info(IDLEWORD[65:64]),
    .data_out(scrambled)
    );
    
//assign scrambled[65:64] = IDLEWORD[65:64];
always @(posedge clk)
begin
 if(cnt == maxcnt)
    begin 
     cnt = 0 ; 
    end
 else 
    begin 
     cnt = cnt +1;
    end
 rxp_reg = scrambled2[cnt];
 rxn_reg = !scrambled2[cnt];
end





















endmodule
