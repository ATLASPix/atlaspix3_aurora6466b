----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11/14/2019 02:18:37 PM
-- Design Name: 
-- Module Name: FIFOwrite_FSM - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity FIFOwrite_FSM is
    Port ( clk : in std_logic;
           data_in : in STD_LOGIC_VECTOR (63 downto 0);
           data_valid : in std_logic;
           data_out : out STD_LOGIC_VECTOR (63 downto 0);
           wr_en : out STD_LOGIC;
           almostfull : in STD_LOGIC;
           empty : in STD_LOGIC;
           trigger_re : in STD_LOGIC;
           busy : out std_logic;
           timestamp : in STD_LOGIC_VECTOR (47 downto 0);
           trigger_timestamp : in STD_LOGIC_VECTOR (47 downto 0)
           );

end FIFOwrite_FSM;

architecture Behavioral of FIFOwrite_FSM is

type state_type is (READ,TRIGGERED,FIFOFULL); 
signal state : state_type;

begin


NEXT_STATE_DECODE  : process(clk)
begin
    if rising_edge(clk) then
    
    case state is 
        when READ =>
            if(almostfull ='1') then 
                state <= FIFOFULL;
            elsif(almostfull = '0' and trigger_re = '0') then 
                state <= READ;
            elsif(trigger_re ='1') then 
                state <= TRIGGERED;
            else
                state <= READ;
            end if;
        when FIFOFULL => 
            if(empty = '0') then 
                state <= FIFOFULL;
            elsif(empty ='1' and trigger_re ='0') then 
                state <= READ;
            elsif(empty ='1' and trigger_re ='1') then 
                state <= TRIGGERED;
            else
                state <= READ;
            end if; 
        when TRIGGERED =>                 
            if(almostfull = '1') then 
                state <= FIFOFULL;
            else
                state <= READ;
            end if; 
        when others =>
                state <= READ;
        end case;        
    end if;
end process;



OUTPUT_DECODE  : process(clk)
begin
    if rising_edge(clk) then

        case state is 
            when READ =>
                busy <= '0';
                wr_en <= '1' and data_valid ;
                data_out <= data_in;
            when TRIGGERED =>
                busy <= '0';
                data_out(47 downto 0) <= trigger_timestamp;
                data_out(63 downto 48) <= X"ABCD";
                wr_en <= '1' ;
            when FIFOFULL =>     
                busy <= '1';
                data_out(47 downto 0) <= timestamp;
                data_out(63 downto 48) <= X"DCBA";
                wr_en <= '1' ;          
            when others => 
                busy <= '0';
      
        end case;
    end if;
end process;



end Behavioral;
